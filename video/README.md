# Ressources Video

[WIMS : le système de notation utilisé pour la remédiation](https://youtu.be/Z_49veO5tpE)

[WIMS : ouverture et fermeture de l'enregistrement des notes](https://youtu.be/aJZ9UGRF1jw)

[WIMS inscription d'un tuteur dans un groupement de classe](https://youtu.be/154M7FkCQ58)

[WIMS : inscription d'un étudiant à une classe](https://youtu.be/bAez8SdaF74)

[WIMS : téléchargement du contenu d'une classe](https://youtu.be/lGPyKC14_jY)

[Lien vers les vidéos du groupe Euler de l'académie de Versailles](https://euler.ac-versailles.fr/rubrique194.html)

