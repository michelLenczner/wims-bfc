# WORKSHOP WIMS BFC 2020

[Programme](https://www.femto-st.fr/fr/workshop-wims-bfc-2020)

[Michel Lenczner et Firmin Varescon Le projet « WIMS en Bourgogne Franche-Comté »](https://youtu.be/BcDRBaBT-ZY)

[David Doyen Présentation générale de WIMS et de ses fonctionnalités](https://youtu.be/530Kiq1oCow)

[Laurence Gigan et Gille Marboeuf « EULER-WIMS », un projet académique dans l'académie de Versailles](https://youtu.be/ZijClSxpkTw)

[Florian Bertrand Les communautés d’apprentissage professionnelles (CAP) : un terreau favorable pour (re)penser les représentations sur l’apprentissage ?](https://youtu.be/FOnLPeXiHMg)

[Csilla Ducrocq WIMS : une utilisation en cours d’anglais](https://youtu.be/4D7agJg_9GY)

[Bruno Mifsud MutuWIMS, quand les utilisateurs s'organisent pour collaborer](https://youtu.be/gNjmsUeej2Y)

[Dominique Revuz Réutilisation de ressources pédagogiques, une expérimentation : PlaTon](https://youtu.be/6r71tx4xHSQ)

[Atelier découverte - Michel Lenczner et Csilla Ducrocq](https://youtu.be/3Q6D61JYXvw)

[Atelier niveau avancé - David Doyen -](https://youtu.be/W8sbv9IVyT8)