# Classes à télécharger dans WIMS

`RemediationUTBM2019-2020.zip` et `RemediationUTBM2020-2021.zip`: les classe WIMS utilisée pour la transition Lycée-Université à l'UTBM durant les premiers semestres 2019-2020 et 2020-2021. Elles ont été construites à partir d'une archive fournie par David Doyen de l'université Gustave Eiffel. La seconde est enrichie avec deux feuilles l'une sur les suites numériques et l'autre sur les ensembles.

`RemediationParParcours2021-2022.tar.gz` : la classe WIMS [presup](https://gitlab.com/michelLenczner/wims-bfc/-/blob/master/fichesExercices/archivesDavidDoyen/archive-presup-17-18.tar.gz) aménagée en tenant compte des cinq parcours en mathématiques au lycée:
- exp : mathématiques expertes
- spe : spécialité mathématique
- comp : mathématiques complémentaires
- prem : arrêt des mathématiques après la première
- sti2d : filière sti2d

Le parcours de chaque étudiant doit être spécifié pour qu'il n'utilise que les exercices qui lui sont adaptés.

`RemediationAvecSequencesEtVitesseUTBM2022-2023.zip` sélection des feuilles de `RemediationParParcours2021-2022.tar.gz` pour les mathématiques expertes et spécialité. Les feuilles sont organisées en séquences avec dans chaque séquence une partie entrainement puis test (basé sur le mode examen) pour des exercices calculatoires adaptés au travail de la vitesse. Le reste des séquences est constitué des autres exercices.
