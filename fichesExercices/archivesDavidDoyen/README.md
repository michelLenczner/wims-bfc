# Archives développées à l'Universite Paris-Est Marne-la-Vallee 

Ces archives ont été développées à l'Universite Paris-Est Marne-la-Vallee et ont été communiquées par David Doyen.

La classe de remédiation pour la transition Lycée - Université est extraite de l'archive `archive-presup-17-18.tar.gz`. 