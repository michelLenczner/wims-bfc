# documents

## Protocole de remédiation

[protocoleRemediation.pdf](protocoleRemediation.pdf) décrit le protocole de remédiation en mathématiques pour la transition Lycée - Université. Il utilise les documents ci-dessous.

## Documents utilisés lors du protocole de remédiation

Les fiches "Fiche_élève" et "Fiche_tuteurs" sont distribuée aux étudiants dans le cadre de remédiations pour la transition Lycée-Université

[Fiche_élève.pdf](Fiche_élève.pdf) :  fiche distribuée aux étudiants

[Fiche_tuteurs.pdf](Fiche_tuteurs.pdf) : fiche distribuée aux tuteurs

[Sondage_de_satisfaction_sur_l_activité_de_remédiation.pdf](Sondage_de_satisfaction_sur_l_activité_de_remédiation.pdf) : sondage de satisfaction effectué à l'issue de la remédiation

[questionnaire.xml](questionnaire.xml) : source xml du sondage de satisfaction (téléchargeable dans Moodle)

[CalculNotes.pdf](CalculNotes.pdf) : principes adoptés pour le calcul des notes

[Aides_pour_WIMS.pdf](Aides_pour_WIMS.pdf) : informations de base, incluant la façon d'écrire les réponses aux exercices.

Les autres documents sont l'[archive d'exercices WIMS](https://gitlab.com/wims-bfc/wims-bfc/-/blob/master/fichesExercices/remediationUTBM2020-2021.zip), le [système de test](https://gitlab.com/wims-bfc/wims-bfc/-/tree/master/test.calcul) et les [tutoriels vidéo](https://gitlab.com/wims-bfc/wims-bfc/-/tree/master/video).

## Autres documents
[Documentation de référence de Sophie Lemaire et Bernadette Perrin-Riou](introProgOEF_Sophie_Bernadette.pdf)

[63 fiches Découvrir Wims créées par l’association WimsEdu](decouvrir_wims_63fiches.pdf)

[90 fiches Découvrir Wims créées par l’association WimsEdu]( decouvrir_wims_90fiches.pdf)
