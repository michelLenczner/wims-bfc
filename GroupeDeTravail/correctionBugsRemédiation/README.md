- [x] 1. Presup-CalculDeDerivee-FonctionUsuellesEtPolynomes-Composition.oef

Feuille 25 (Dérivées)

**12/07/2021 : nos modifications ont été prises en compte par David**

- [ ] 2. Presup-ComplexesModulesArguments-CalculConnaissantModuleArgument.oef

**12/07/2021 : Essai de publication comme module par Tobit**

Attente de création compte de la part de publish.wims@unice.fr

- [ ] 3. Presup-Graphe-de-Fonction-Trigonometrique.oef

Feuille 28 exo 5, 6 (feuille complète). Ces exercices sont supprimés dans la mise à jour de la remédiation 2021-2022.

**12/07/2021 : signalement du bug**

- [ ] 4. Presup-NombresComplexesFormeAlgebrique-AffixesOperations.oef

Ex 4 feuille 20 (Nombres complexes en forme algébrique)

**12/07/2021 : signalement du bug**

- [ ] 5. Presup-Puissances-et-racines-carrées.oef

**12/07/2021 : signalé à David avec lien vers le fichier corrigé du dépôt Gitlab**

- [ ] 6. Presup-Suites-Suites_récurrentes_et_sens_de_variation.oef

Feuille 23, Exercice 5

**12/07/2021 : signalement du bug**

- [ ] 7. Presup-TrinomeSecondDegré-FormeCanonique.oef 

Feuille 8, Exercice 5?

**12/07/2021 : signalement du bug**
