# WIMS Exercice sur les IPP
Contient
- introhook
- ipp.cpp

## TODO:
- IPP:
    - Faire un paramètre pour aide ou non aide (donnée de u et v')
    - Vérifier les étapes au delà de la première
    - Faire avec calcul final ou sans calcul (seulement primitive)

- Exercice intégrale changement de variable


## Introhook

!set introhook_exists=yes

!set introhool_tableless=yes

!set wims_html_mode=li
<ul>
!formcheckbox confparm1 list 1,2,3 prompt Ln, Exp, Trigo
</ul>
Degré maximal du polynôme associé au logarithme : 
<ul>
!formradio confparm2 list 0,1,2,3 prompt 0, 1, 2, 3
</ul>




## src/cpp/ipp.cpp

```c=

target= ippguide ippNonGuide

#if defined TARGET_ippguide
\title{Intégration par parties guidé}
#endif

#if defined TARGET_ippNonGuide
\title{Intégration par parties non guidé}
#endif

\language{fr}
\author{Groupe WIMS UBFC}
\email{tobit.caudwell@u-bourgogne.fr}

\integer{a=random(-1,1)*randint(1..5)}
\integer{b=random(-10..10)}
# bornes
\integer{m=0}
\integer{M=\m+randint(1..5)}

# Cas par défaut
\if{\confparm1=}{
# type (ax+b)*exp
\integer{k=random(-1,1)*randint(1..5)}
\function{u=maxima(\a*x+\b)}
\function{v1=maxima(exp(\k*x))}
\function{fonc=(\u)*(\v1)}
}

# Cas avec logarithme
\if{\confparm1=1}{
# type polynome*ln
\text{degpoly = randint(0..\confparm2)}
\text{poly=slib(polynomial/random \degpoly,5,x,monic)}
\integer{k=randint(1..3)}
\function{u=maxima(ln(\k*x))}
\function{v1=maxima(\poly)}
\function{fonc=(\v1)*(\u)}
\integer{m=1}
\integer{M=\m+randint(1..3)}
}

% Cas avec fonction exponentielle
\if{\confparm1=2}{
% type (ax+b)*exp
\integer{k=random(-1,1)*randint(1..5)}
\function{u=maxima(\a*x+\b)}
\function{v1=maxima(exp(\k*x))}
\function{fonc=(\u)*(\v1)}
}

% Cas avec fonction trigo
\if{\confparm1=3}{
% type (ax+b)*sin-cos
\integer{k=random(-1,1)*randint(1..5)}
\function{u=maxima(\a*x+\b)}
\function{vsin=maxima(sin(\k*x))}
\function{vcos=maxima(cos(\k*x))}
\function{v1=randitem(\vsin,\vcos)}
\function{fonc=(\u)*(\v1)}
\integer{m=0}
\integer{M=randint(2,3,4,6)}
\text{M = \pi/\M}
}





\text{fonc = wims(replace internal log by ln in \fonc)}
\text{tfonc=texmath(\fonc)}
\text{integrale=maxima(int(\fonc,x,\m,\M))}

\function{u1=maxima(diff(\u,x))}
\function{v=simplify(maxima(int(\v1,x)))}
\function{fonc2=maxima((\u1)*(\v))}
\text{intpart=maxima(int((\u1)*(\v),x,\m,\M))}
\text{crochet=maxima(subst(\M,x,(\u)*(\v))-subst(\m,x,(\u)*(\v)))}
\text{texintpart=texmath(\intpart)}
\text{texcrochet=texmath(wims(replace internal log by ln in \crochet))}
\text{texfonc2=texmath(\fonc2)}
% remplacement de log par ln
\text{u = wims(replace internal log by ln in \u)}


\steps{
reply 1,reply 2
reply 3
reply 4
reply 5
}



\statement{<em>Cet exercice comporte quatre étapes.</em><br><br>
On considère la fonction \(f) définie par \(f(x)=\tfonc).<br>
On se propose d'utiliser une intégration par parties pour calculer :
<div class="wimscenter">\(I=\int_{\m}^{\M} f(x)dx = \int_{\m}^{\M}\tfonc dx)</div>

<div>Pour cela, on pose :
<div class="wimscenter">\(u(x)=\u) &emsp; et &emsp; \(v'(x)=\v1).</div>
</div>
\if{\step=1}{
<em> Question 1 : </em>
On calcule alors :</br> 
\(u'(x)=) \embed{reply 1, 10} et
\(v(x)=) \embed{reply 2, 10} .
}

\if{\step>=2}{<em> Question 1 : </em> \(u'(x)=\u1) &emsp; et &emsp; \(v(x)=\v) </br>}
\if{\step=2}{<em> Question 2 : </em> Dans ces conditions, on calcule :  
<div class="wimscenter">  \([u(x)v(x)]_\m^{\M}=) \embed{reply 3,30}
</div>
}

\if{\step>=3}{<em> Question 2 : </em> \([u(x)v(x)]_\m^{\M}=\texcrochet) </br>}
\if{\step=3}{<em> Question 3 : </em> Enfin, on calcule l'intégrale \(\int_\m^\M u'(x)v(x)dx=\int_\m^{\M} \texfonc2 dx = ) \embed{reply 4,30}.<br>
}

\if{\step>=4}{<em> Question 3 : </em> \(\int_\m^{\M} u'(x)v(x)dx=\texintpart) </br>}
\if{\step=4}{<em> Question 4 : </em> D'après ce qui précède, on peut finalement calculer :
<div class="wimscenter">\(I=) \embed{reply 5, 30}
</div>}
}

\answer{u1}{\u1}{type=function}{option=nonstop}
\answer{v}{\v}{type=function}{option=nonstop}
\answer{Crochet}{\crochet}{type=formal}{option=nonstop}
\answer{Intégrale partielle}{\intpart}{type=formal}{option=nonstop}
\answer{Intégrale}{\integrale}{type=formal}

\hint{On rappelle que \(\int_a^b u(x)v'(x)dx=[u(x)v(x)]_a^b- \int_a^b u'(x)v(x)dx)}

```
