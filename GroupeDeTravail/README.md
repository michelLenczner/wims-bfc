# Groupe de Travail

## CorrectionBugRemediation
Les fichiers .oef de la remédiation Lycée-Supérieur qui ont été corrigés lors de groupes de Travail

## ipp.md
Fichier .md contenant l'exercice en cours de développement.

##  Tableau_synoptique_cycle_terminal
Fichiers .pdf et .doc (word) de description du programmes de maths pour les différents parcours.
[Lien vers le google doc](https://docs.google.com/spreadsheets/d/1ysBYq75J3sFw26VQi2p7eq9Np60fIbaPBChZHOzgfV4/edit#gid=0).
